(function () {
	angular.module('medirest.controllers', [
		'medirest.controllersLogin',
		'medirest.controllersHome',
		'medirest.controllersWelcome',
		'medirest.controllersMenus',
		'medirest.controllersOrder',
		'medirest.controllersFinish',
		'medirest.controllersAdmin',
	])
})()
