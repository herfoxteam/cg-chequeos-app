(function () {
	angular.module('medirest.routes', [])
		.config(['$stateProvider', '$urlRouterProvider',
			function ($stateProvider, $urlRouterProvider) {
				$stateProvider
					.state('login', {
						url: '/login',
						cache: false,
						templateUrl: 'templates/login.html',
						controller: 'loginController'
					})
					.state('home', {
						url: '/home',
						templateUrl: 'templates/home.html',
						cache: false,
						controller: 'homeController'
					})
					.state('welcome', {
						url: '/welcome',
						templateUrl: 'templates/welcome.html',
						controller: 'welcomeController'
					})
					.state('menus', {
						url: '/menus',
						cache: false,
						templateUrl: 'templates/menus.html',
						controller: 'menusController'
					})
					.state('lunch', {
						url: '/lunch',
						templateUrl: 'templates/lunch.html',
						controller: 'menusController'
					})
					.state('order', {
						url: '/order',
						templateUrl: 'templates/order.html',
						controller: 'orderController'
					})
					.state('finish', {
						url: '/finish',
						templateUrl: 'templates/finish.html',
						controller: 'finishController'
					})
					.state('admin', {
						url: '/admin',
						templateUrl: 'templates/admin.html',
						controller: 'adminController'
					})

				$urlRouterProvider.otherwise('/login');
			}
		])

})()