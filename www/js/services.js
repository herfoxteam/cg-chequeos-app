(function () {
	angular.module('medirest.services', [])
		.factory('customersService', ['$http', '$q', '$rootScope', '$httpParamSerializerJQLike', function ($http, $q, $rootScope, $httpParamSerializerJQLike) {

			function all(locationId) {
        if (locationId === undefined)
          locationId = JSON.parse(localStorage.getItem('locationId'))
				var deferred = $q.defer()
				var req = {
					method: 'GET',
					url: $rootScope.url + 'api/customers?search=location_id:' + locationId,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					}
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}
			function create(customer) {
				var deferred = $q.defer()
				var req = {
					method: 'POST',
					url: $rootScope.url + 'api/customers',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					},
					data: $httpParamSerializerJQLike(customer)
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}
			function update(customer) {
				var deferred = $q.defer()
				var req = {
					method: 'PUT',
					url: $rootScope.url + 'api/customers/' + customer.id,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					},
					data: $httpParamSerializerJQLike(customer)
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}
			function filterDni(dni,locationId) {
				var deferred = $q.defer()
				var req = {
					method: 'GET',
					url: $rootScope.url + 'api/customers?search=dni:' + dni + ';location_id:' + locationId + '&searchJoin=and',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					}
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			function filterPlace(place, locationId) {
				var deferred = $q.defer()
				var req = {
					method: 'GET',
					url: $rootScope.url + 'api/customers?search=place_id:' + place + ';location_id:' + locationId + '&searchJoin=and',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					}
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				all: all,
				create: create,
				update: update,
				filterDni: filterDni,
				filterPlace: filterPlace
			}
		}])

		.factory('placesService', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
			function all() {
				var deferred = $q.defer()
				var req = {
					method: 'GET',
					url: $rootScope.url + 'api/places',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					}
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				all : all
			}
		}])

		.factory('locationsService', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
			function all(locationId) {
				var deferred = $q.defer()
				var req = {
					method: 'GET',
					url: $rootScope.url + 'api/locations/' + locationId,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					}
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				all : all
			}
		}])

		.factory('menusService', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
			function menu(menuId, diet_id) {
				var deferred = $q.defer()
        let diet = (diet_id ? ';diet_id:' + diet_id : '')
				var req = {
					method: 'GET',
					url: $rootScope.url + 'api/menus?search=menu_id:' + menuId + diet,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': 'Bearer ' + $rootScope.token
					}
				}
				$http(req)
					.success(function (data) {
						for (i = 0; i < data.data.categories.length; i++) {
							data.data.categories[i].checked = false
							for (j = 0; j < data.data.categories[i].products.length; j++) {
								for (k = 0; k < data.data.categories[i].products[j].complements.length; k++) {
									if (k === 0)
										data.data.categories[i].products[j].complements[k].actual = true
									else
										data.data.categories[i].products[j].complements[k].actual = false
								}
							}
							for (j = 0; j < data.data.categories[i].subcategories.length; j++) {
								for (k = 0; k < data.data.categories[i].subcategories[j].products.length; k++) {
									for (l = 0; l < data.data.categories[i].subcategories[j].products[k].complements.length; l++) {
										if (l === 0)
											data.data.categories[i].subcategories[j].products[k].complements[l].actual = true
										else
											data.data.categories[i].subcategories[j].products[k].complements[l].actual = false
									}
								}
							}
						}
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				menu : menu
			}
		}])

		.factory('loginService', ['$http', '$q', '$rootScope', '$httpParamSerializerJQLike', function ($http, $q, $rootScope, $httpParamSerializerJQLike) {
			function all(user) {
				// console.log(user)
				var deferred = $q.defer()
				var req = {
					method: 'POST',
					url: $rootScope.url + 'api/login',
					headers: {
						'Authorization': 'Bearer ' + $rootScope.token,
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: $httpParamSerializerJQLike(user)
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				all : all
			}
		}])

		.factory('serviceService', ['$http', '$q', '$rootScope', '$httpParamSerializerJQLike', function ($http, $q, $rootScope, $httpParamSerializerJQLike) {
			function send(data) {
				// console.log(data)
				var deferred = $q.defer()
				var req = {
					method: 'POST',
					url: $rootScope.url + 'api/services',
					headers: {
						'Authorization': 'Bearer ' + $rootScope.token,
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: $httpParamSerializerJQLike(data)
				}
				$http(req)
					.success(function (data) {
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				send : send
			}
		}])

		.factory('authService', ['$http', '$q', '$rootScope', '$httpParamSerializerJQLike', function ($http, $q, $rootScope, $httpParamSerializerJQLike) {
			function all() {
				var deferred = $q.defer()
				var req = {
					method: 'POST',
					url: 'http://54.147.84.201/oauth/token',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: $httpParamSerializerJQLike({
						grant_type: 'password',
						client_id: 1,
						client_secret: '5Uuks1Ot5V8ukC7bEeLVHjL9g4ZI3zGUXcsoKpC1',
						username: 'herfox@live.com',
						password: '123456'
					})
				}
				$http(req)
					.success(function (data) {
						$rootScope.token = data.access_token
						localStorage.setItem("token", JSON.stringify($rootScope.token))
						$http.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(localStorage.getItem("token"))
						deferred.resolve(data)
					})
					.error(function(err) {
						deferred.reject(err)
					})

				return deferred.promise
			}

			return {
				all : all
			}
		}])

		.factory('$ImageCacheFactory', ['$q', function($q) {
			return {
					Cache: function(urls) {
							if (!(urls instanceof Array))
									return $q.reject('Input is not an array');

							var promises = [];

							for (var i = 0; i < urls.length; i++) {
									var deferred = $q.defer();
									var img = new Image();

									img.onload = (function(deferred) {
											return function(){
													deferred.resolve();
											}
									})(deferred);

									img.onerror = (function(deferred,url) {
											return function(){
													deferred.reject(url);
											}
									})(deferred,urls[i]);

									promises.push(deferred.promise);
									img.src = urls[i];
							}

							return $q.all(promises);
					}
			}
		}])

})()
