// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('medirest', [
  'ionic',
  'ngCordova',
  'medirest.controllers',
  'medirest.services',
  'medirest.routes',
  'ionicLazyLoad'
])

  .run(function ($ionicPlatform, $rootScope, $state, $ionicHistory, $http, $location, $ImageCacheFactory) {

    $ImageCacheFactory.Cache([
      'img/bg-welcome.jpg'
    ])
    $ionicPlatform.registerBackButtonAction(function (event) {
      event.preventDefault();
      // $rootScope.categories = JSON.parse(localStorage.getItem('categories'))
      if ($location.path() === "/home") {
        navigator.app.exitApp();
      } else if ($location.path() === "/menus" || $location.path() === "/lunch") {

        // If el paso actual es 0 ir a welcome
        if ($rootScope.actualStep === 0)
          if($rootScope.categories[$rootScope.actualStep].subcategories.length > 0)
            if ($state.current.url === '/lunch')
              $state.go('welcome')
            else
              $state.go('lunch')
          else
            $state.go('welcome')
        else {
          $rootScope.actualStep--
          if ($rootScope.categories[$rootScope.actualStep] !== undefined) {
            if ($rootScope.categories[$rootScope.actualStep + 1].subcategories.length > 0) {
              if ($state.current.url !== '/lunch') {
                $rootScope.actualStep++
                $state.go('lunch')
              }
            }
            if ($rootScope.categories[$rootScope.actualStep].subcategories.length > 0 ) {
              $state.go('lunch')
            }
            // Si categoria no tiene subcategoris como (pescados, aves, etc...)
            else if ($rootScope.categories[$rootScope.actualStep].subcategories.length === 0) {
              $state.reload()
              $state.go('menus')
            }
          }
          else {
            $rootScope.actualStep++
            $state.go('welcome')
            console.log('ohh por dios jaja')
          }
        }
      }
      // else if ($location.path() === "/welcome") {
      //   $rootScope.categories = JSON.parse(localStorage.getItem('categories'))
      //   $state.go('home')
      // }
      else {
        $ionicHistory.goBack();
      }
    }, 100)

    // $http.defaults.headers.common.Content_Type = 'application/x-www-form-urlencoded'
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(localStorage.getItem("token"))
    $rootScope.token = JSON.parse(localStorage.getItem('token'))
    $rootScope.url = 'http://54.147.84.201/'
    // $rootScope.url = 'https://herfox-test.s3.amazonaws.com/medirest/images/'
    $rootScope.countError = 0

    $rootScope.evaluateError = function (error) {
      // Options default error
      if (error === null) {
        var options = {
          title: 'Error de Conexion!',
          template: 'Comprueba tu conexión a internet',
          doneAlert: function () {
            $state.go('login')
          }
        }
        $rootScope.showAlert(options)
      }
      if (error) {
        if (error.error === 'Unauthenticated.') {
          var options = {
            title: 'Error de Autenticación!',
            template: 'Vuelve a loguearte',
            doneAlert: function () {
              $ionicHistory.clearCache().then(function () {
                $ionicHistory.clearHistory()
                $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true })
                $state.go('login')
              })
            }
          }
          localStorage.removeItem('token')
          localStorage.removeItem('actualCustomer')
          localStorage.removeItem('locationId')
          localStorage.removeItem('menuId')
        }
        else if (error.message === 'Invalid email') {
          options = {
            title: 'Correo Inválido, por favor verifique',
            template: error.error,
            doneAlert: function () { }
          }
        }
        else if (error.message === 'Invalid password') {
          options = {
            title: 'Contraseña Inválida, por favor verifique',
            template: error.error,
            doneAlert: function () { }
          }
        }
        else if (error.message === 'invalid_client') {
          options = {
            title: 'Acceso Invalido',
            template: error.error,
            doneAlert: function () { }
          }
        }
        else if (error.message === 'The dni must be a number.') {
          options = {
            title: 'Identificación invalida',
            template: error.error,
            doneAlert: function () { }
          }
        }
        else {
          options = {
            title: 'Contacte al administrador',
            template: error.error,
            doneAlert: function () { }
          }
        }
        $rootScope.showAlert(options)
      }
    }

    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs).
      // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
      // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
      // useful especially with forms, though we would prefer giving the user a little more room
      // to interact with the app.
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(false);
      }
      if (window.StatusBar) {
        // Set the statusbar to use the default style, tweak this to
        // remove the status bar on iOS or change it to use white instead of dark colors.
        // StatusBar.styleDefault();
        // StatusBar.overlaysWebView(true);
        StatusBar.overlaysWebView (false)
        // StatusBar.styleLightContent();
        StatusBar.backgroundColorByHexString('#27324C');
      }
      // ionic.Platform.fullScreen();
    })

  })
  .directive('search', function () {
    return {
      restrict: 'E',
      templateUrl: 'templates/partials/search.html'
    }
  })
  .directive('expired', [ '$ionicPopup', '$rootScope', '$location', function ($ionicPopup, $rootScope, $location) {
    return {
      restrict: 'E',
      // templateUrl: 'templates/partials/expired.html',
      controller: function ($scope) {
        $rootScope.showAlert = function(options) {
          var alertPopup = $ionicPopup.alert({
            title: options.title,
            cssClass: 'popupErrors',
            template: options.template
          });

          alertPopup.then(function(res) {
            options.doneAlert()
          })
        }
      }
    }
  }])
  .filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  })
