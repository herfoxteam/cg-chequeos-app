(function () {
	angular.module('medirest.controllersLogin', [])
		.controller('loginController', [
			'$scope',
			'$stateParams',
			'$state',
			'$rootScope',
			'$ionicLoading',
			'loginService',
			'authService',
			'locationsService',
			function (
				$scope,
				$stateParams,
				$state,
				$rootScope,
				$ionicLoading,
				loginService,
				authService,
				locationsService
			) {
				if (localStorage.token && localStorage.locationId && localStorage.menuId)
					$state.go('home')
				else {
					$scope.showSpinner = function () {
						$ionicLoading.show({
							template: '<ion-spinner icon="lines" class="spinner-energized"></ion-spinner>',
							noBackdrop: true
						})
					}
					$scope.showSpinner()
					authService.all()
						.then(function (response) {
							$ionicLoading.hide()
							// $rootScope.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjIwNzc4Y2M4NWQzMTM2MTg1ZTVkNTFmZDIyMWRmMzJmNDM5MTgxMTQ0NDA3ZTIzMGU4MmI0ZmZiMzY2MjFkMTUwYzBjM2RhN2M2YTc5Mjg0In0.eyJhdWQiOiIxIiwianRpIjoiMjA3NzhjYzg1ZDMxMzYxODVlNWQ1MWZkMjIxZGYzMmY0MzkxODExNDQ0MDdlMjMwZTgyYjRmZmIzNjYyMWQxNTBjMGMzZGE3YzZhNzkyODQiLCJpYXQiOjE1MDI1ODIyMDQsIm5iZiI6MTUwMjU4MjIwNCwiZXhwIjoxNTM0MTE4MjA0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.JZBypOHrbzB1-G-Aweoqm33JJQNiyO58xlfMbXQesgPbiivwfXRz7M1mbnz8Hbm2RbGxgr_2uN85sDVPWGoCixqkHyHmgAk6KG5h5tRrY81fj8Is2qiBnlFxYX49zycKNkgP1ZepRZc2GfSODtTG9IkWjI65bj6wx3opUSZi-K0uUcP5C14oRYxzAnegbwLl6ThpJ-tEjqCEsx9CRqea4FvU60rrRTJlDCOtO8R8CKF35YwpxNpyqS0b2vt_WHUEA5KNgBAnwcj8ZeeTvwWNA-Q5IQ2SjyDKFRNAq2Wgnd0_obssnqYTz3KMPwCfxIhnLHVaiE6nh-ZRiTd_frTGdKVhv-Thlpk4hb4KION7fGNGg2Qc6hb9dDrssqDGeCIQCvg9V0QO1X_BJBeCYYM8x0oKdhnRApOLX2hZW6-mlmb0mNxb6UFvnwrRflfG7vjelM22Q2QysVjnY6aPjShsHzsFGjj_0KlALhAtr7tZfXLLBSiITB_vLffKOxuTan6X8cmHe--HOchJayb6AzyCqgz6rYhdE5diRYYjQPkM0ZQAcJ3gLnA_WBvrn84N5u9o8qmrq5aO4s1bqAVLc8qg3BhrWS6n_cHRFPO8V0R2QTxJK753y1G3wT5hzVootm0gvn5H1l1o0vIkciEJso74r2s3XU929DcM9wrufos1FBs'
							// $rootScope.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE2OGIyZWNmODgzYjU5OTdkOThjZDg4ZmVjNWE5ODI1Yzk0NGIwYTE5YTdmODJjZDk3NDYyMTYwMmFmYmY1ZTI3OGMwMjFjZDFhM2MzMzgwIn0.eyJhdWQiOiIxIiwianRpIjoiYTY4YjJlY2Y4ODNiNTk5N2Q5OGNkODhmZWM1YTk4MjVjOTQ0YjBhMTlhN2Y4MmNkOTc0NjIxNjAyYWZiZjVlMjc4YzAyMWNkMWEzYzMzODAiLCJpYXQiOjE1MDM1OTU3MTUsIm5iZiI6MTUwMzU5NTcxNSwiZXhwIjoxNTM1MTMxNzE1LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.STH3BNP8mj8Yns66Y2wm3s6mm0f-LM4cs0BQh10Tm_JBFMwAuFIpnFTSPVIu1yQS1EKFs6N4pGw1f2lIvGQiveYqhkQxcgQ2oZJc1a-4IYEEt99mGFuz1ioZhCDM3--fAg0Obv5K4bQzHO0G3B4n70xEKsKZHEEQBZhLRDJbd7Si27WOP0c3L1sbEmTILUEx4R5pZzHYjury1nSbmfLBjaVKMdJL84D7ZdOxELGcTT-z0hyRvkcuC4UfAteBD5luPZefChTCSjoYVZQt-4oRv6ILVIhSSFASYw73TyHsDH7uaK4LBsVIqTz_tulG7UwY4D9lLso4JvEYv5v-GDooYnTv6IOw63In9D0hsO1bQIS52b9QK9Woc-rMqGFul6avgfg2Y4kdfv6qB4M5jDPCYnokZ2nUp_yDjWzjY_JUts0yl7e6QSnCSCQroSj3Lb-xzIxX9TWJGT-kfnbqBBhgxGanNgejBsYMg4aq3H-qMq26P9x2HYc73ufama9C-2gIG_Q0PbWNEbzugNp8evY16JXd6XUSC82VwFcTV99D7Ck6ge0SApjrtPXIbxClK2t8ckh6IfeHVxnzNNWnkgaoTKYgD3NJN74hknCO-Kvjb89bAdSSu4J_Yy5TPYauu-9wJGgwrttUtVEZQtahBrsiCGqByw6AqacmGY2Ly5ADl1M'
						}, function (reason) {
							$rootScope.evaluateError(reason)
							$ionicLoading.hide()
						})
				}

				$scope.user = {}
				$scope.login = function () {
					$scope.showSpinner()
					loginService.all($scope.user)
						.then(function (response) {
							if (response.success == true) {
								$scope.user = response.data
                $rootScope.actualImageLocation = response.data.location.logo
                $rootScope.actualNameLocation = response.data.location.name
								$rootScope.actualLocationId = response.data.location_id
                localStorage.setItem('imageLocation', JSON.stringify($rootScope.actualImageLocation))
                localStorage.setItem('nameLocation', JSON.stringify($rootScope.actualNameLocation))
								localStorage.setItem("locationId", JSON.stringify($rootScope.actualLocationId))
								localStorage.setItem("email", JSON.stringify($scope.user.email))

								locationsService.all($rootScope.actualLocationId)
									.then(function (response) {
										$rootScope.menuId = response.data.menu_id
										localStorage.setItem("menuId", JSON.stringify($rootScope.menuId))
										$ionicLoading.hide()
										$state.go('home')
									}, function (reason) {
										$rootScope.evaluateError(reason)
										$ionicLoading.hide()
									})
							}
							else {
								$rootScope.evaluateError(response)
								$ionicLoading.hide()
							}
						}, function (reason) {
							$rootScope.evaluateError(reason)
							$ionicLoading.hide()
						})
				}
			}
		])
})()
