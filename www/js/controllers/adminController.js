(function () {
	angular.module('medirest.controllersAdmin', [])
		.controller('adminController', [
			'$scope',
			'$rootScope',
			'$state',
			'$ionicLoading',
			'serviceService',
			'loginService',
			function (
				$scope,
				$rootScope,
				$state,
				$ionicLoading,
				serviceService,
				loginService
			) {

				// Loading
				$scope.showSpinner = function () {
					$ionicLoading.show({
						template: '<ion-spinner icon="lines" class="spinner-energized"></ion-spinner>',
						noBackdrop: true
					})
				}

				$scope.backStep = function () {
					$state.go('order')
				}

				$rootScope.actualEmail = JSON.parse(localStorage.getItem('email'))

        if ($rootScope.actualImageLocation === undefined)
          $rootScope.actualImageLocation = JSON.parse(localStorage.getItem('imageLocation'))
        if ($rootScope.actualNameLocation === undefined)
          $rootScope.actualNameLocation = JSON.parse(localStorage.getItem('nameLocation'))

				$scope.user = {
					email: $rootScope.actualEmail
				}

				$scope.login = function () {
					$scope.showSpinner()
					if ($scope.user.email === undefined)
						$scope.user.email = JSON.parse(localStorage.getItem('email'))
					loginService.all($scope.user)
						.then(function (response) {
							if (response.success == true) {
								// Build order final
								var products = []
								for (i = 0; i < $rootScope.actualOrder.length; i++) {
									var complements = []
									for (j = 0; j < $rootScope.actualOrder[i].accompaniments.length; j++) {
										complements.push({
											complement_id: $rootScope.actualOrder[i].accompaniments[j].complement_id,
											product_id: $rootScope.actualOrder[i].accompaniments[j].id
										})
									}
									products.push({
										product_id: $rootScope.actualOrder[i].id,
										category_id: $rootScope.actualOrder[i].category_id,
										complements: JSON.stringify(complements),
										observations: $rootScope.actualOrder[i].comment
									})
								}
								var info = $rootScope.actualCustomer || JSON.parse(localStorage.getItem('actualCustomer'))

								var serviceOrder = {
									location_id: info.location_id,
									place_id: info.place_id,
									customer_id: info.id,
									observations: $scope.user.comments,
									products: products
								}

								serviceService.send(serviceOrder)
									.then(function (response) {
										setTimeout(function () {
											$ionicLoading.hide()
											$scope.user = {}
											$state.go('home')
										}, 4000);
									}, function (reason) {
										debugger
										$rootScope.evaluateError(reason)
										$ionicLoading.hide()
									})
							}
							else {
								$rootScope.evaluateError(response)
								$ionicLoading.hide()
							}
						}, function (reason) {
							$rootScope.evaluateError(reason)
							$ionicLoading.hide()
						})
				}
			}
		])
})()
