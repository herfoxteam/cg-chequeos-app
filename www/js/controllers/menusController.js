(function () {
	angular.module('medirest.controllersMenus', [])
		.controller('menusController', [
			'$scope',
			'$rootScope',
			'$stateParams',
			'$state',
			'$ionicModal',
			'$ionicLoading',
			'$ionicPopup',
			'customersService',
			'placesService',
			'menusService',
			function (
				$scope,
				$rootScope,
				$stateParams,
				$state,
				$ionicModal,
				$ionicLoading,
				$ionicPopup,
				customersService,
				placesService,
				menusService
			) {
				// Loading
				$scope.showSpinner = function () {
					$ionicLoading.show({
						template: '<ion-spinner icon="lines" class="spinner-energized"></ion-spinner>',
						noBackdrop: true
					})
				}

				if ($rootScope.actualStep === undefined) $rootScope.actualStep = 0
				if ($rootScope.actualOrder === undefined) $rootScope.actualOrder = []
				// if ($rootScope.categories === undefined) $rootScope.categories = JSON.parse(localStorage.getItem('categories'))

				$scope.data = {
					comment: '',
				}

				// Select menu or product
				$scope.selectMenu = function (menu) {
					// Validacion de caracteres de compelmentos
					for (i = 0; i < menu.complements.length; i++) {
						for (j = 0; j < menu.complements[i].complement_products.length; j++) {
							if (menu.complements[i].complement_products[j].name.length > 25) {
								menu.complements[i].long = true
								j = menu.complements[i].complement_products.length
							}
						}
					}

					// Validacion de categoria ya existente en la orden actual
					for (i = 0; i < $rootScope.actualOrder.length; i++) {
						if ($rootScope.actualOrder[i].category == $rootScope.categories[$rootScope.actualStep].name) {
							$rootScope.actualOrder.splice(i, 1)
						}
					}

					// Armando menu actual
					$scope.actualMenu = {
						id: menu.id,
						name: menu.name,
						description: menu.description,
						image: menu.image,
						complements: menu.complements,
						category: $rootScope.categories[$rootScope.actualStep].name,
						category_id: $rootScope.categories[$rootScope.actualStep].id,
            observation: (JSON.parse(localStorage.getItem('actualCustomer')).observation !== null) ? JSON.parse(localStorage.getItem('actualCustomer')).observation : 'Ninguna'
					}

					// Si categoria tiene acompañamientos mostrar modal de acompañamientos
					if (menu.complements.length > 0) $scope.menuOpenModal()
					else $scope.summaryOpenModal()
				}

				// Select checkbox option
				$scope.updateSelection = function (position, items, name) {
					angular.forEach(items, function (item, index) {
						if (position != index)
							item.checked = false
						$scope.selected = name
					})
				}

				$scope.summary = {}
				// Armar resumen
				var buildSummary = function () {
					var actualAccompaniments = []

					// Armar array de acompañantes de la categoria actual
					for (i = 0; i < $scope.actualMenu.complements.length; i++) {
						for (j = 0; j < $scope.actualMenu.complements[i].complement_products.length; j++) {
							if ($scope.actualMenu.complements[i].complement_products[j].checked) {
								actualAccompaniments.push({
									title: $scope.actualMenu.complements[i].name,
									complement_id: $scope.actualMenu.complements[i].id,
									name: $scope.actualMenu.complements[i].complement_products[j].name,
									id: $scope.actualMenu.complements[i].complement_products[j].id,
								})
							}
						}
					}

					// Armar data del resumen de la categoria actual
					$scope.summary.accompaniments = actualAccompaniments

					$scope.menuCloseModal()
					$scope.summaryOpenModal()
				}

				// prevModal summary
				$scope.prevModal = function () {
					if ($scope.actualMenu.complements.length > 0) {
						$scope.summaryCloseModal()
						$scope.menuOpenModal()
					} else $scope.summaryCloseModal()
				}

				// Volver btn
				$scope.prevAccompaniment = function () {
					var lengthComplements = $scope.actualMenu.complements.length
					if ($scope.actualMenu.complements[0].actual) {
						$scope.menuCloseModal()
					} else {
						for (i = 0; i < lengthComplements; i++) {
							if ($scope.actualMenu.complements[i].actual) {
								$scope.actualMenu.complements[i].actual = false
								$scope.actualMenu.complements[i - 1].actual = true
								i = lengthComplements
							}
						}
					}
				}

				// Omitir btn
				$scope.skipAccompaniment = function () {
					// Si es el ultimo elemento del array
					var lengthComplements = $scope.actualMenu.complements.length
					if ($scope.actualMenu.complements[lengthComplements - 1].actual) {
						for (i = 0; i < lengthComplements; i++) {
							if ($scope.actualMenu.complements[i].actual) {
								for (j = 0; j < $scope.actualMenu.complements[i].complement_products.length; j++) {
									$scope.actualMenu.complements[i].complement_products[j].checked = false
								}
							}
						}
						buildSummary()
					} else {
						for (i = 0; i < lengthComplements; i++) {
							if ($scope.actualMenu.complements[i].actual) {
								for (j = 0; j < $scope.actualMenu.complements[i].complement_products.length; j++) {
									$scope.actualMenu.complements[i].complement_products[j].checked = false
								}
								if ($scope.actualMenu.complements[i + 1]) {
									$scope.actualMenu.complements[i].actual = false
									$scope.actualMenu.complements[i + 1].actual = true
									i = lengthComplements
								}
							}
						}
					}
				}

				// Siguiente btn
				$scope.nextAccompaniment = function () {
					$scope.selectedItem = false
					// Si es el ultimo elemento del array
					var lengthComplements = $scope.actualMenu.complements.length
					for (i = 0; i < lengthComplements; i++) {
						if ($scope.actualMenu.complements[i].actual) {
							for (j = 0; j < $scope.actualMenu.complements[i].complement_products.length; j++) {
								if ($scope.actualMenu.complements[i].complement_products[j].checked) {
									$scope.selectedItem = true
								}
							}
						}
					}

					// Si no se selecciono ningun complemento
					if (!$scope.selectedItem) {
						$scope.showAlert = function() {
							var alertPopup = $ionicPopup.alert({
								title: 'No seleccionaste ninguna opción',
								template: 'Por favor selecciona un item o Omite',
								cssClass: 'popupErrors'
							})
						}
						$scope.showAlert()
					}
					else {
						if ($scope.actualMenu.complements[lengthComplements - 1].actual) {
							// Var establecida - cierre de modal automatico
							$scope.autoExitModal = true
							buildSummary()
						} else {
							// Recorriendo array de acompañamientos
							for (i = 0; i < lengthComplements; i++) {
								if ($scope.actualMenu.complements[i].actual) {
									// Si hay mas acompañantes
									if ($scope.actualMenu.complements[i + 1]) {
										$scope.actualMenu.complements[i].actual = false
										$scope.actualMenu.complements[i + 1].actual = true
										i = lengthComplements
									}
								}
							}
						}
					}
				}

				// Pasar a siguiente categoria y guardar
				$scope.nextCategory = function (summary) {
					// Update Order
					$scope.actualMenu.category = $rootScope.categories[$scope.actualStep].name
					$rootScope.actualStep++
					$scope.actualMenu.accompaniments = $scope.summary.accompaniments || []
					$scope.actualMenu.comment = $scope.data.comment
					$rootScope.actualOrder.push($scope.actualMenu)
					$scope.data = {comment: ''}
					/********************************* */
					localStorage.setItem("test", JSON.stringify($rootScope.actualOrder))
					/********************************* */
					$scope.summaryCloseModal()
					$scope.summary.accompaniments = []
					if ($rootScope.categories[$rootScope.actualStep] !== undefined) {
						// Si categoria no tiene subcategoris como (pescados, aves, etc...)
						if ($rootScope.categories[$rootScope.actualStep].subcategories.length > 0)
							$state.go('lunch')
					}
					else {
						$rootScope.actualStep--
						$state.go('order')
					}
				}

				// Modal select menu
				$ionicModal.fromTemplateUrl('templates/select-menu.html', {
					scope: $scope,
					controller: 'selectMenuCtrl',
					animation: 'slide-in-up',//'slide-left-right', 'slide-in-up', 'slide-right-left'
					focusFirstInput: false
				}).then(function (modal) {
					$scope.menuModal = modal
				})

				// Modal menus open
				$scope.menuOpenModal = function () {
					$scope.menuModal.show()
				}

				// Fixed remove accompaniments actual
				$scope.resetAccompaniments = function () {
					if ($scope.actualMenu) {
						if ($scope.actualMenu.complements) {
							var lengthComplements = $scope.actualMenu.complements.length
							for (i = 0; i < lengthComplements; i++) {
								if (i == 0)
									$scope.actualMenu.complements[i].actual = true
								else
									$scope.actualMenu.complements[i].actual = false
								for (j = 0; j < $scope.actualMenu.complements[i].complement_products.length; j++) {
									$scope.actualMenu.complements[i].complement_products[j].checked = false
								}
							}
						}
					}
				}

				// Event close modal
				$scope.$on('modal.hidden', function (e, info) {
					if (info.controller === 'selectMenuCtrl') {
						// Si  no quizo finalizar, borra todo y vuelve al accompañamiento 0
						if (!$scope.autoExitModal)
							$scope.resetAccompaniments()
						$scope.autoExitModal = false
					}
				})

				// Modal menus close
				$scope.menuCloseModal = function () {
					$scope.menuModal.hide()
				}

				// Cleanup the modal when we're done with it! detecta cambios
				$scope.$on('$destroy', function () {
					$scope.menuModal.remove()
				})

				// Modal summary
				$ionicModal.fromTemplateUrl('templates/summary.html', {
					scope: $scope,
					controller: 'type2lController',
					animation: 'slide-in-up',//'slide-left-right', 'slide-in-up', 'slide-right-left'
					focusFirstInput: false
				}).then(function (modal) {
					$scope.summaryModal = modal
				})

				// Modal summary open
				$scope.summaryOpenModal = function () {
					$scope.summaryModal.show()
				}

				// Modal summary close
				$scope.summaryCloseModal = function () {
					$scope.summaryModal.hide()
				}

				// Cleanup the modal when we're done with it! detecta cambios
				$scope.$on('$destroy', function () {
					$scope.summaryModal.remove()
				})

				$scope.selectSubcategory = function (item) {
					$rootScope.subProducts = item.products
					$state.go('menus')
				}

				$scope.backStep = function () {
					// reset modal
					$scope.resetAccompaniments()

					// If el paso actual es 0 ir a welcome
					if ($rootScope.actualStep === 0)
						if($rootScope.categories[$rootScope.actualStep].subcategories.length > 0)
							if ($state.current.url === '/lunch')
								$state.go('welcome')
							else
								$state.go('lunch')
						else
							$state.go('welcome')
					else {
						$rootScope.actualStep--
						if ($rootScope.categories[$rootScope.actualStep] !== undefined) {
							if ($rootScope.categories[$rootScope.actualStep + 1].subcategories.length > 0) {
								if ($state.current.url !== '/lunch') {
									$rootScope.actualStep++
									$state.go('lunch')
								}
							}
							if ($rootScope.categories[$rootScope.actualStep].subcategories.length > 0 ) {
								$state.go('lunch')
							}
							// Si categoria no tiene subcategoris como (pescados, aves, etc...)
							else if ($rootScope.categories[$rootScope.actualStep].subcategories.length === 0) {
								$state.go('menus')
							}
						}
						else {
							$rootScope.actualStep++
							$state.go('welcome')
							console.log('ohh por dios jaja')
						}
					}
				}

			}
		])
})()
