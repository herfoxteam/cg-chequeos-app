(function () {
	angular.module('medirest.controllersWelcome', [])
		.controller('welcomeController', [
			'$scope',
			'$rootScope',
			'$state',
			function (
				$scope,
				$rootScope,
				$state
			) {
				if (!(localStorage.token && localStorage.locationId && localStorage.menuId)) $state.go('login')
				if ($rootScope.actualCustomer === undefined) $rootScope.actualCustomer = JSON.parse(localStorage.getItem('actualCustomer'))

				// Arrow back
				$scope.backStep = function () {
					$state.go('home')
				}

        if ($rootScope.actualImageLocation === undefined)
          $rootScope.actualImageLocation = JSON.parse(localStorage.getItem('imageLocation'))
        if ($rootScope.actualNameLocation === undefined)
          $rootScope.actualNameLocation = JSON.parse(localStorage.getItem('nameLocation'))

				$scope.nextStep = function () {
					$rootScope.actualStep = 0
					$rootScope.actualOrder = []
					// if ($rootScope.categories === undefined)
						// $rootScope.categories = JSON.parse(localStorage.getItem('categories'))
					if ($rootScope.categories[$rootScope.actualStep].subcategories.length > 0)
						$state.go('lunch')
					else
						$state.go('menus')

				}
			}
		])
})()
