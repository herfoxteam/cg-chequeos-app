(function () {
	angular.module('medirest.controllersFinish', [])
		.controller('finishController', [
			'$scope',
			'$rootScope',
			'$stateParams',
			'$state',
			'$ionicModal',
			'$ionicLoading',
			'customersService',
			'placesService',
			'menusService',
			function (
				$scope,
				$rootScope,
				$stateParams,
				$state,
				$ionicModal,
				$ionicLoading,
				customersService,
				placesService,
				menusService
			) {
				if($rootScope.actualCustomer === undefined)
					$rootScope.actualCustomer = JSON.parse(localStorage.getItem('actualCustomer'))

        if ($rootScope.actualImageLocation === undefined)
          $rootScope.actualImageLocation = JSON.parse(localStorage.getItem('imageLocation'))
        if ($rootScope.actualNameLocation === undefined)
          $rootScope.actualNameLocation = JSON.parse(localStorage.getItem('nameLocation'))

				// if ($rootScope.actualOrder === undefined) $state.go('home')
				// if ($rootScope.actualOrder === undefined)
					// $rootScope.actualOrder = JSON.parse(localStorage.getItem('test'))
				$scope.goToAdmin = function () {
					$state.go('admin')
				}
			}
		])
})()
