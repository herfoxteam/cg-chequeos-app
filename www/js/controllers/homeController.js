(function () {
	angular.module('medirest.controllersHome', [])
		.controller('homeController', [
			'$scope',
			'$rootScope',
			'$stateParams',
			'$state',
			'$ionicModal',
			'$ionicPopup',
			'$ionicLoading',
			'customersService',
			'placesService',
			'locationsService',
			'menusService',
			function (
				$scope,
				$rootScope,
				$stateParams,
				$state,
				$ionicModal,
				$ionicPopup,
				$ionicLoading,
				customersService,
				placesService,
				locationsService,
				menusService
			) {
				if (!(localStorage.token && localStorage.locationId && localStorage.menuId)) $state.go('login')
				$scope.validForm = false
				$rootScope.categories = undefined
				$rootScope.actualOrder = undefined
				$scope.showSpinner = function () {
					$ionicLoading.show({
						template: '<ion-spinner icon="lines" class="spinner-energized"></ion-spinner>',
						noBackdrop: true
					})
				}
				$scope.showSpinner()

				$scope.userExisting = true

        if ($rootScope.actualLocationId === undefined)
          $rootScope.actualLocationId = JSON.parse(localStorage.getItem('locationId'))
        if ($rootScope.actualImageLocation === undefined)
          $rootScope.actualImageLocation = JSON.parse(localStorage.getItem('imageLocation'))
        if ($rootScope.actualNameLocation === undefined)
          $rootScope.actualNameLocation = JSON.parse(localStorage.getItem('nameLocation'))

				$rootScope.actualCustomer = {
					firstname: 'nombre',
					dni: 'identificación',
					place: 'select'
				}

				// Si no hay categorias en $scope, hacer request
				if ($rootScope.menuId === undefined)
					$rootScope.menuId = JSON.parse(localStorage.getItem('menuId'))

        $ionicLoading.hide()

        $scope.getMenu = function(diet_id) {
          $scope.showSpinner()
          menusService.menu($rootScope.menuId, diet_id)
            .then(function (response) {
              if (response.success) {
                $rootScope.categories = response.data.categories
                localStorage.setItem("categories", JSON.stringify($rootScope.categories))
                $rootScope.copyCategories = JSON.parse(localStorage.getItem('categories'))
                var cacheImg = []
                for (i = 0; i < $rootScope.categories.length; i++) {
                  if ($rootScope.categories[i].products.length > 0) {
                    for (j = 0; j < $rootScope.categories[i].products.length; j++) {
                      // var item = $rootScope.url + $rootScope.categories[i].products.image
                      cacheImg.push($rootScope.url + $rootScope.categories[i].products[j].image)
                    }
                  } else if ($rootScope.categories[i].subcategories.length > 0) {
                    for (j = 0; j < $rootScope.categories[i].subcategories.length; j++) {
                      if ($rootScope.categories[i].subcategories[j].products.length > 0) {
                        for (k = 0; k < $rootScope.categories[i].subcategories[j].products.length; k++) {
                          cacheImg.push($rootScope.categories[i].subcategories[j].products[k].image)
                        }
                      }
                    }
                  }
                }
              }
              $ionicLoading.hide()
            }, function (reason) {
              $rootScope.evaluateError(reason)
              $ionicLoading.hide()
            }
          )
        }
				$scope.searchDniNow = false
				// Get customers
				customersService.all($rootScope.actualLocationId)
					.then(function (response) {
						$scope.customers = response.data
						// Get places
						placesService.all()
							.then(function (response) {
								$scope.places = response.data
							}, function (reason) {
								$rootScope.evaluateError(reason)
								$ionicLoading.hide()
							})
					}, function (reason) {
						$rootScope.evaluateError(reason)
						$ionicLoading.hide()
					})

				$scope.search = {}

				$scope.refreshCustomers = function () {
					$scope.showSpinner()
					$scope.search.dni=''
					customersService.all()
						.then(function (response) {
							$scope.customers = response.data
							$ionicLoading.hide()
						}, function (reason) {
							$rootScope.evaluateError(reason)
							$ionicLoading.hide()
						})
				}


				// Modal select category
				$ionicModal.fromTemplateUrl('templates/select-customer.html', {
					scope: $scope,
					controller: 'typeController',
					animation: 'slide-in-up',//'slide-left-right', 'slide-in-up', 'slide-right-left'
					focusFirstInput: false
				}).then(function (modal) {
					$scope.customerModal = modal
					})

				// Modal category open
				$scope.customerOpenModal = function () {
					$scope.customerModal.show()
				}

				// Modal category close
				$scope.customerCloseModal = function () {
					$scope.customerModal.hide()
				}

				// Cleanup the modal when we're done with it! detecta cambios
				$scope.$on('$destroy', function () {
					$scope.customerModal.remove()
				})

				// Edit Dni
				$scope.changeDni = function () {
					if ($rootScope.actualCustomer.dni != 'identificación') {

						$rootScope.data = {
							actualFirstName: $rootScope.actualCustomer.firstname,
							actualLastName: $rootScope.actualCustomer.lastname,
							actualDni: $rootScope.actualCustomer.dni
						}
						var alertPopup = $ionicPopup.show({
							title: 'Actualizar Datos',
							cssClass: 'popupDni',
							template: '<label class="item item-input">' +
													'<input type="text" ng-model="data.actualFirstName">' +
												'</label>' +
												'<label class="item item-input">' +
													'<input type="text" ng-model="data.actualLastName">' +
												'</label>' +
												'<label class="item item-input">' +
													'<input type="text" ng-model="data.actualDni">' +
												'</label>',
							scope: $rootScope,
							buttons: [
								{ text: 'Cancelar' },
								{
									text: 'Actualizar',
									type: 'button-assertive',
									onTap: function (e) {
										$scope.showSpinner()
										$rootScope.actualCustomer.firstname = $rootScope.data.actualFirstName
										$rootScope.actualCustomer.lastname = $rootScope.data.actualLastName
										$rootScope.actualCustomer.dni = $rootScope.data.actualDni
										var customer = {
											id: $rootScope.actualCustomer.id,
											firstname: $rootScope.actualCustomer.firstname,
											lastname: $rootScope.actualCustomer.lastname,
											dni: $rootScope.actualCustomer.dni,
											location_id: $rootScope.actualCustomer.location_id,
											place_id: $rootScope.actualCustomer.place_id
										}
										customersService.update(customer)
											.then(function (response) {
												if (response.success) {
													$rootScope.actualCustomer = response.data
												}
												$ionicLoading.hide()
											}, function (reason) {
												$rootScope.evaluateError(reason)
												$ionicLoading.hide()
											})
										alertPopup.close();
									}
								}
							]
						})
					}

				}

				// Search by Place
				$scope.searchByPlace = function (place) {
					customersService.filterPlace(place, $rootScope.actualLocationId)
						.then(function (response) {
							if (response.data.length > 0) {
								$scope.customers = response.data
							} else {
								$scope.customers = [{firstname: 'No se encontraron resultados'}]
							}
							$scope.searchDniNow = true
							console.log($scope.searchDniNow)
						}, function (reason) {
							$rootScope.evaluateError(reason)
							$ionicLoading.hide()
						})
				}

				// Search by Dni
				$scope.searchDni = function (dni) {
					customersService.filterDni(dni, $rootScope.actualLocationId)
						.then(function (response) {
							if (response.data.length > 0) {
								$scope.customers = response.data
							} else {
								$scope.customers = [{firstname: 'No se encontraron resultados'}]
							}
							$scope.searchDniNow = true
							console.log($scope.searchDniNow)
						}, function (reason) {
							$rootScope.evaluateError(reason)
							$ionicLoading.hide()
						})
				}

				// Select customer
				$scope.selectCustomer = function (customer) {
					$rootScope.actualCustomer = customer
					localStorage.setItem("actualCustomer", JSON.stringify(customer))
					if ($rootScope.actualCustomer && $rootScope.actualCustomer.place_id)
						$scope.validForm = true
          $scope.getMenu(customer.diet_id)
          console.log($rootScope.actualCustomer)
					$scope.customerModal.hide().then(function(){
            $ionicPopup.show({
              title: 'Observaciones',
              cssClass: 'popupDni',
              template: '<div class="observation">{{actualCustomer.observation}}</div>',
              scope: $rootScope,
              buttons: [
                { text: 'Cerrar'}
              ]
            })

          })
				}

				// Modal select place
				$ionicModal.fromTemplateUrl('templates/select-place.html', {
					scope: $scope,
					controller: 'typeController',
					animation: 'slide-in-up',//'slide-left-right', 'slide-in-up', 'slide-right-left'
					focusFirstInput: false
				}).then(function (modal) {
					$scope.placeModal = modal
				})

				// Modal place open
				$scope.placeOpenModal = function (isNew, toFilter) {
					$scope.placeToFilter = toFilter;
					$scope.placeModal.show()
				}

				// Modal place close
				$scope.placeCloseModal = function () {
					$scope.placeModal.hide()
				}

				// Cleanup the modal when we're done with it! detecta cambios
				$scope.$on('$destroy', function () {
					$scope.placeModal.remove()
				})

				// Select place
				$scope.selectPlace = function (place) {
					if ($scope.placeToFilter){
						$scope.search.place = place;
						$scope.placeModal.hide()
					} else {
						$rootScope.actualCustomer.place_id = place.id
						$scope.newCustomer.placeId = place.id
						$scope.placeModal.hide()
						if ($rootScope.actualCustomer && $rootScope.actualCustomer.place_id)
							$scope.validForm = true
					}
				}

				// Validate options categories
				$scope.validateOptions = function () {
					for(var i = $rootScope.categories.length - 1; i>=0 ;i--){
						if($rootScope.categories[i].checked == false)
							$rootScope.categories.splice(i,1)
					}
					if ($rootScope.categories.length === 0) {
						$rootScope.categories = JSON.parse(localStorage.getItem('categories'))
						var alertPopup = $ionicPopup.alert({
							title: 'Debe seleccionar categoria',
							template: 'Por favor selecciona una o varias categorias',
							cssClass: 'popupErrors'
						})
					} else return true
				}

				// Create customer
				$scope.newCustomer = {}
				$scope.createCustomer = function () {
					var willNext = $scope.welcome()
					if (willNext) {
						$scope.showSpinner()

						if ($rootScope.actualLocationId === undefined)
							$rootScope.actualLocationId = JSON.parse(localStorage.getItem('locationId'))

						var customer = {
							firstname: $scope.newCustomer.name,
							lastname: $scope.newCustomer.lastName,
							dni: $scope.newCustomer.dni,
							location_id: $rootScope.actualLocationId,
							place_id: $scope.newCustomer.placeId
						}

						customersService.create(customer)
							.then(function (response) {
								if (response.success) {
									if (response.message == 'Customer already exist!') {
										$rootScope.actualCustomer = response.data[0]
										localStorage.setItem("actualCustomer", JSON.stringify($rootScope.actualCustomer))
										var alertPopup = $ionicPopup.show({
											title: 'Usuario ya existe',
											cssClass: 'popupDni',
											template: '<div style="color: white;"><p>Este usuario ya existe, desea continuar ?</p><div><p>Nombre: {{actualCustomer.firstname}}</p><p>Apellido: {{actualCustomer.lastname}}</p><p>Identificación: {{actualCustomer.dni}}</p><p>Sala: {{actualCustomer.place_id}}</p></div></div>',
											scope: $rootScope,
											buttons: [
												{
													text: 'Cancelar',
													onTap: function (e) {
														$rootScope.categories = JSON.parse(localStorage.getItem('categories'))
													}
												},
												{
													text: 'Aceptar',
													type: 'button-assertive',
													onTap: function (e) {
														alertPopup.close()
														$state.go('welcome')
													}
												}
											]
										})
									} else if (response.message == 'Customer saved successfully') {
										$rootScope.actualCustomer = response.data
										$state.go('welcome')
									}
								}
								$ionicLoading.hide()
							}, function (reason) {
								$rootScope.categories = JSON.parse(localStorage.getItem('categories'))
								$ionicLoading.hide()
								$rootScope.evaluateError(reason)
							})
					}
				}

				// Go to welcome
				$scope.welcome = function (existUser) {
					if (existUser) {
						if ($scope.validateOptions())
							$state.go('welcome')
					} else {
						if ($scope.validateOptions())
							return true
						else
							return false
					}
				}
			}
		])
})()
