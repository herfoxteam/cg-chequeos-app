(function () {
	angular.module('medirest.controllersOrder', [])
		.controller('orderController', [
			'$scope',
			'$rootScope',
			'$stateParams',
			'$state',
			'$ionicModal',
			'$ionicLoading',
			'customersService',
			'placesService',
			'menusService',
			function (
				$scope,
				$rootScope,
				$stateParams,
				$state,
				$ionicModal,
				$ionicLoading,
				customersService,
				placesService,
				menusService
			) {
				// if ($rootScope.actualOrder === undefined) $state.go('home')
				if ($rootScope.actualOrder === undefined)
					$rootScope.actualOrder = JSON.parse(localStorage.getItem('test'))
				console.log($rootScope.actualOrder)

        if ($rootScope.actualImageLocation === undefined)
          $rootScope.actualImageLocation = JSON.parse(localStorage.getItem('imageLocation'))
        if ($rootScope.actualNameLocation === undefined)
          $rootScope.actualNameLocation = JSON.parse(localStorage.getItem('nameLocation'))

				$scope.prevModal = function () {
					$state.go('menus')
				}
				$scope.cancel = function () {
					$rootScope.actualStep = 0
					$state.go('home')
				}
				$scope.finish = function () {
					$state.go('finish')
				}
			}
		])
})()
