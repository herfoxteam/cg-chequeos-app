Ionic App Base
==============

A starting project for Ionic that optionally supports using custom SCSS.

## Using this project

We recommend using the [Ionic CLI](https://github.com/ionic-team/ionic-cli) to create new Ionic projects that are based on this project but use a ready-made starter template.

For example, to start a new Ionic project with the default tabs interface, make sure the `ionic` utility is installed:

```bash
$ npm install -g ionic cordova
```

Then run:

```bash
$ ionic start myProject tabs --type=ionic1
```

More info on this can be found on the Ionic [Getting Started](https://ionicframework.com/getting-started) page and the [Ionic CLI](https://github.com/ionic-team/ionic-cli) repo.

## Issues

Issues have been disabled on this repo. If you do find an issue or have a question, consider posting it on the [Ionic Forum](https://forum.ionicframework.com/). If there is truly an error, follow our guidelines for [submitting an issue](https://ionicframework.com/submit-issue/) to the main Ionic repository.

// flex-wrap: wrap

-webkit-flex-wrap: wrap;
flex-wrap: wrap;

// align-items: center

-webkit-box-align: center;
-webkit-align-items: center;
align-items: center;

// justify-content: center

-webkit-box-pack:center;
-webkit-justify-content:center;
justify-content: center;


// justify-content:space-around

-webkit-box-pack:justify;
-webkit-justify-content:space-around;
justify-content:space-around;


// justify-content:space-between

-webkit-box-pack:justify;
-webkit-justify-content:space-between;
justify-content:space-between;


## Fixeds

- $rootScope.actualStep cuando es 3
- Validar si existe imagen, si no, cargar imagen por defecto
